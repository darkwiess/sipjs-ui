import React, { useEffect, useState } from 'react';
import { Inviter, InviterOptions, SessionState, UserAgent, Registerer, RegistererOptions, Web  } from "sip.js";
import './App.css';
/* eslint-disable */
import adapter from 'webrtc-adapter';
var userAgent = null, local_video, remote_video, sessionDescriptionHandler;

function App() {
    const callNumber = "bob.4gDrtezaeZnVvBW14z1gAjUcBw2wimiB";
    const [registerNumber, setRegisterNumber] = useState(localStorage.getItem("registerNumber"));
    const [isRegister, setIsRegister] = useState(false)

    useEffect(() => {
        local_video  = document.getElementById("local-video");
        remote_video = document.getElementById("remote-video");
    },[])

    const initSIP = () => {
        localStorage.setItem("registerNumber", registerNumber)
        userAgent = new UserAgent({
            uri: UserAgent.makeURI(`sip:test12345@sipjs.onsip.com`),
            register : true,
            noAnswerTimeout : 10000,
            authorizationUser : registerNumber,
            transportOptions: {
                connectionTimeout : 200000, 
                server: "wss://edge.sip.onsip.com",
                traceSip : true
            },
        });

        const registererOptions: RegistererOptions = {  };
        const registerer = new Registerer(userAgent, registererOptions);
        userAgent.start().then(() => { 
            setIsRegister(true) 
            registerer.register();
        });

        userAgent.delegate = {
            onInvite(invitation: Invitation): void {
          
                // An Invitation is a Session
                const incomingSession: Session = invitation;
          
                // Setup incoming session delegate
                incomingSession.delegate = {
                  // Handle incoming REFER request.
                  onRefer(referral: Referral): void {
                    // ...
                  }
                };
          
                // Handle incoming session state changes.
                incomingSession.stateChange.addListener((newState: SessionState) => {
                  switch (newState) {
                    case SessionState.Establishing:
                      // Session is establishing.
                      break;
                    case SessionState.Established:
                      // Session has been established.
                      break;
                    case SessionState.Terminated:
                      // Session has terminated.
                      break;
                    default:
                      break;
                  }
                });
          
                // Handle incoming INVITE request.
                let constrainsDefault: MediaStreamConstraints = {
                    audio: false,
                    video: true,
                }
          
                const options: InvitationAcceptOptions = {
                    sessionDescriptionHandlerOptions: {
                      constraints: constrainsDefault,
                    },
                }
          
                incomingSession.accept(options)
            }
        };
    }

    const makeCall = () => {
        const target = UserAgent.makeURI(`sip:${callNumber}@sipjs.onsip.com`);
        if (!target) {
            throw new Error("Failed to create target URI");
        }
        const options : InviterOptions = {
            sessionDescriptionHandlerOptions: {
                constraints: { 
                    audio: true, 
                    video: true,
                }
            }
        }
        const inviter = new Inviter(userAgent, target, options);
    
        // Handle outgoing session state changes
        inviter.stateChange.addListener((newState) => {
            switch (newState) {
                case SessionState.Establishing:
                break;
                case SessionState.Established:
                    sessionDescriptionHandler = inviter.sessionDescriptionHandler;
                    if (!sessionDescriptionHandler || !(sessionDescriptionHandler instanceof Web.SessionDescriptionHandler)) {
                        throw new Error("Invalid session description handler.");
                    }
                    if (local_video)  { assignStream(sessionDescriptionHandler.localMediaStream, local_video);}
                    if (remote_video) { assignStream(sessionDescriptionHandler.remoteMediaStream, remote_video);}
                break;
                case SessionState.Terminated:
                // Session has terminated
                break;
                default:
                break;
            }
        });
        const assignStream = (stream: MediaStream, element: HTMLMediaElement) => {
            element.autoplay = true; // Safari does not allow calling .play() from a non user action
            element.srcObject = stream;
            element.play().catch((error: Error) => {
                console.error("Failed to play media");
                console.error(error);
            });
            stream.onaddtrack = (): void => {
                element.load(); // Safari does not work otheriwse
                element.play().catch((error: Error) => {
                    console.error("Failed to play remote media on add track");
                    console.error(error);
                });
            };
            stream.onremovetrack = (): void => {
                element.load(); // Safari does not work otheriwse
                element.play().catch((error: Error) => {
                    console.error("Failed to play remote media on remove track");
                    console.error(error);
                });
            };
        }
       
        inviter.invite()
        .then(() => {
        })
        .catch((error: Error) => {
        });
    }

    const _handleMakeCall = () => {
        if(isRegister === true){
            makeCall();
        }
    }
    const _handleRegister = () => {
        initSIP();
    }
    const _handelRegisterNumber = (event) => {
        setRegisterNumber(event.target.value)
    }
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-sm">
                    <div className="d-grid gap-12">
                        <input type="text" placeholder="registernumber" onChange={_handelRegisterNumber} value={"test12345"} />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-sm">
                    <div className="d-grid gap-12">
                        <input type="text" value={callNumber} placeholder="callnumber" />
                    </div>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-sm">
                    <div className="d-grid gap-2">
                        <button className="btn btn-outline-secondary" onClick={_handleRegister} type="button">Register</button>
                    </div>
                </div>
                <div className="col-sm">
                    <div className="d-grid gap-2">
                        <button className="btn btn-outline-primary" onClick={_handleMakeCall} type="button">Call</button>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-sm">
                    <div className="d-grid gap-2">
                        <div className="card" >
                            <div className="card-body">
                                <video id="local-video" className="local-video" muted></video>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm">
                    <div className="d-grid gap-2">
                        <div className="card" >
                            <div className="card-body">
                                <video id="remote-video" className="remote-video" muted></video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
 